-- :name push-history! :! :n
-- :doc logs a query to the history table
INSERT INTO queries
(id, date, operation)
VALUES (:id, :date, :operation);

-- :name get-history-event :? :1
-- :doc get a query from the history
SELECT * FROM queries
WHERE id = :id;

-- :name get-history :? :*
-- :doc get query history
SELECT * FROM queries
ORDER BY date DESC
LIMIT :depth;

