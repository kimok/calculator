CREATE TABLE users
(id VARCHAR(20) PRIMARY KEY,
 first_name VARCHAR(30),
 last_name VARCHAR(30),
 email VARCHAR(30),
 admin BOOLEAN,
 last_login TIMESTAMP,
 is_active BOOLEAN,
 pass VARCHAR(300));

CREATE TABLE queries
(id VARCHAR(36) PRIMARY KEY,
 date VARCHAR(36),
 operation VARCHAR(MAX));
