(ns kimok.calculator.routes.home
  (:require
   [kimok.calculator.layout :as layout]
   [kimok.calculator.db.core :refer [*db*] :as db]
   [next.jdbc :as jdbc]
   [kimok.calculator.math :as math]
   [clojure.java.io :as io]
   [kimok.calculator.middleware :as middleware]
   [ring.util.http-response :as http-response]
   [ring.middleware.json :as ring-json]
   [clojure.data.json :as json]))

(defn home-page [request]
  (layout/render request "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page [request]
  (layout/render request "about.html"))

(defn calculator [{:keys [params]}]
  (try
    (http-response/ok (assoc params
                             :result
                             (str (math/parse-math (:expression params)))))
    (catch Exception e (http-response/internal-server-error (str e)))))

(defn maxprod3  [{:keys [params]}]
  (try
    (http-response/ok
     (assoc params
            :result
            (str (math/max-product-of-3 (:numbers params)))))
    (catch Exception e (http-response/internal-server-error (str e)))))

(defn store-history [handler]
  (fn [request]
    (let [id (.toString (java.util.UUID/randomUUID))
          response (handler request)]
      (if-not (= (:status response) 200)
        response
        (do
         (jdbc/with-transaction [t-conn *db*]
           (db/push-history!
            t-conn
            {:id id
             :date (str (java.util.Date.))
             :operation (json/write-str (:body response))}))
         (assoc-in response [:body :id] id))))))

(defn history-event [id]
  (jdbc/with-transaction [t-conn *db*]
    (db/get-history-event t-conn {:id id} {})))


(defn history [{{:keys [id depth]} :params}]
  (jdbc/with-transaction [t-conn *db*]
    (let [body (cond
                 id [(db/get-history-event t-conn {:id id} {})]
                 depth (db/get-history t-conn {:depth depth} {})
                 true [(first (db/get-history t-conn {:depth 1} {}))])]
      (http-response/ok (json/write-str body)))))

(defn home-routes []
  [ "" 
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/math" {:middleware [store-history]}
    ["/calculator" {:get calculator}]                    
    ["/maxprod3" {:get maxprod3}]]
   ["/history" {:get history}]])
