(ns kimok.calculator.math
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as string]))

(s/def ::digit (set "0123456789.n"))
(s/def ::operator (s/cat :kw #{:operator} :val char?))
(s/def ::expression (s/* (s/alt :number number?
                                :operator #{\/ \* \+ \-}
                                :parenthetical ::parenthetical)))
(s/def ::parenthetical 
       (s/cat :open #{\(}
              :body (s/* (s/alt :number number?
                                :operator #{\/ \* \+ \-}
                                :parenthetical ::parenthetical))
              :close #{\)}))

(def order-of-operations
  [[:operator \*]
   [:operator \/]
   [:operator \+]
   [:operator \-]])

(defn some-index [coll ops]
      (if (empty? ops) nil
          (let [i (.indexOf coll (first ops))]
            (if (= i -1)
              (some-index coll (rest ops))
              i))))

(defn evaluate [expression]
  (let [index (some-index expression order-of-operations)
        or-paren #(let [e (Exception. (str "invalid arithmetic at " %))]
                    (case (first %)
                    :parenthetical (-> % second :body evaluate)
                    :number (second %)
                    (throw e)
                    ))]
    (if-not index (or-paren (first expression))
            (let [[left-hand operator right-hand :as operation]
                  (take 3 (drop (dec index) expression))
                  before (take (dec index) expression)
                  after (drop (+ 2 index) expression)
                  result [:number ((resolve (symbol (str (second operator))))
                                   (or-paren left-hand)
                                   (or-paren right-hand))]]
              (evaluate
               (concat before
                       [result]
                       after))))))

(defn parse-math [arithmetic]
  (->>
   (string/replace
    (string/replace arithmetic #" " "")  #"(^|[^0-9])-([0-9]+)" "$1n$2")
   seq       
   (partition-by #(s/valid? ::digit %))
   (map #(if (s/valid? ::digit (last %))
           (Integer/parseInt (string/replace (apply str %) #"n" "-"))
           %))
   flatten
   (s/conform ::expression)
   evaluate))

(defn max-product-of-3 [coll]
  (if-not (> (count coll) 2)
    (throw (Exception.
            "not enough numbers to find greatest product of three")))
  (let [coll (sort coll)]
        (max (apply * (take-last 3 coll))
             (apply * (take 3 (concat (take 2 coll)
                                      (reverse coll)))))))
