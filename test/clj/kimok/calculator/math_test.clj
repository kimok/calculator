(ns kimok.calculator.math-test
  (:require
   [clojure.test :refer :all]
   [kimok.calculator.math :refer :all]))

(deftest test-parse-math
  (is (= -4 (parse-math "-1 * (2 * 6 / 3)")))
  (is (= 1/2 (parse-math "1/2")))
  (is (= -1/2 (parse-math "1/-2")))
  (is (= 42 (parse-math "(((((((((((42)))))))))))")))
  (is (try (parse-math "2+*2")
           (catch Exception e true))))

(deftest test-max-product-of-3
  (is (= 300 (max-product-of-3 [1, 10, 2, 6, 5, 3])))
  (is (= 100 (max-product-of-3 [-2 -2 25 1 2])))
  (is (= -6 (max-product-of-3 [-1 -2 -3 -4])))
  (is (= -24 (max-product-of-3 [3 4 -2]))))
