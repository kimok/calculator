(ns kimok.calculator.db.core-test
  (:require
   [kimok.calculator.db.core :refer [*db*] :as db]
   [java-time.pre-java8]
   [luminus-migrations.core :as migrations]
   [clojure.test :refer :all]
   [next.jdbc :as jdbc]
   [kimok.calculator.config :refer [env]]
   [mount.core :as mount]
   [clojure.data.json :as json]))

(use-fixtures
  :once
  (fn [f]
    (mount/start
     #'kimok.calculator.config/env
     #'kimok.calculator.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (f)))

(deftest test-db
      (let [id (.toString (java.util.UUID/randomUUID))
            date (str (java.util.Date.))]
        (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
          (is (= 1 (db/push-history!
                    t-conn
                    {:id id
                     :date date
                     :operation (json/write-str {:expression "2 + 2" :result "4"})})))
          (is (= {:id id
                  :date date
                  :operation (json/write-str {:expression "2 + 2" :result "4"})}
                 (db/get-history-event t-conn {:id id} {}))))))
