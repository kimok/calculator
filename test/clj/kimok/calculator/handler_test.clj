(ns kimok.calculator.handler-test
  (:require
    [clojure.test :refer :all]
    [ring.mock.request :refer :all]
    [kimok.calculator.handler :refer :all]
    [kimok.calculator.middleware.formats :as formats]
    [muuntaja.core :as m]
    [mount.core :as mount]
    [clojure.data.json :as json]))

(defn parse-json [body]
  (m/decode formats/instance "application/json" body))

(use-fixtures
  :once
  (fn [f]
    (mount/start #'kimok.calculator.config/env
                 #'kimok.calculator.handler/app-routes)
    (f)))

(deftest test-app
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 200 (:status response)))))

  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response)))))
  (testing "calculator route"
    (let [response ((app)
                    (-> (request :get "/math/calculator")
                        (json-body {:expression "-1 * (2 * 6 / 3)"})))
          result (:result (parse-json (:body response)))]
      (is (= "-4" result))))
  (testing "max product of 3 route"
    (let [response ((app)
                    (-> (request
                         :get "/math/maxprod3")
                        (json-body {:numbers [1 10 2 6 5 3]})))
          result (:result (parse-json (:body response)))]
      (is (= "300" result))))
  (testing "history-event"
    (let [expression "2+2"
          result "4"
          math-response
          ((app)
           (-> (request :get "/math/calculator")
               (json-body {:expression expression})))
          id (-> math-response :body parse-json :id)
          history-response ((app)
                            (-> (request
                                :get "/history")
                                (json-body {:id id})))
          operation (-> history-response :body parse-json :operation parse-json)]
      (is (= {:expression "2+2", :result "4"}
             operation))))
  (testing "history-depth"
    (let [expression "2+2"
          result "4"
          math-response
          ((app)
           (-> (request :get "/math/calculator")
               (json-body {:expression expression})))
          history-response ((app) (request
                                   :get "/history"
                                   {:depth 3}))
          operations (-> history-response :body parse-json)]
      (is (= 3
             (count operations))))))
